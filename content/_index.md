## Info:
- Version: 1.15.2 (Java only)
- Server IP: `server.sneegsnag.com`

At the moment, the only public facing server is Vanilla. It should be noted that only subs have access to this server while they are currently subbed and cheating. 
***(x-raying, mods, etc), griefing, bullying, etc will NOT be tolerated at any time and is subject to be banned not just on the server, but in discord and twitch chat as well.***

## Rules:
- Don't be a dick
- No griefing
- Sexism, Racism, Homophobia = ban
- No chat spam
- Use common sense
- Don't start drama
- No self advertising
- No NSFW shit
    - Dicks are permitted
- Impersonation = ban
- Have fun
    - If you don't have fun you will be banned
        - (This is a joke by the way)
- Pls report anyone breaking da rules :}

### Whitelisting:
In order to be whitelisted you MUST fill out the form below. Filling out this form will make you eligible to access BOTH subservers, but you do not have to play on both if you do not desire. 
This does not automatically whitelist you, mods will have to look over the forms then automatically whitelist so please be patient. 
If you are not whitelisted after 1 week, then feel free to message or @ a mod in Discord.

[Whitelist Form](https://docs.google.com/forms/d/e/1FAIpQLSdFtQ9wMW244plya75HKccAR7RlRjIv68QkUpoGqd0ThaRR4w/viewform)