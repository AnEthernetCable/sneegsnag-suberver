---
title: 1.16 IS ON THE SUBSERVER AAAAAAAA
date: 2020-06-28
---
1.16.1 is the official subserver version! As I am writing this, we are waiting for the subserver to do a backup.

- Changes:
	- Server version is now 1.16.1. Bugs will probably exist.
	- Nether has been reset. Go get that netherite.

- Still looking into:
	- More advanced logging of actions on the server to solve disputes and issues faster. Again ty for being awesome
	- Issue tracking and bug reporting feature. For now, just use `/mail`, Discord, or carrier pidgeon to let me know.
	- Maybe bats will stop fucking lagging the server. Will see in this new update.

Enjoy 1.16 yall
Especially you mole :}.
