---
title: Zero-tick farms and lag
date: 2020-06-20
---
- Updated the waterfall version to v348. This shouldnt change anything on yall's side of things
- Updated the server jar to v348:
    - This should fix zero-tick farms, let me know if not.
    - Should also improve lag issues and crashes
- Added lag prevention plugins
    - It will not delete items or anything of the sorts, however will unload chunks much faster than normal. Again shouldn't be noticable by yall. Lemme know if it causes issues :}

### Upcoming:
- Implement more anti-cheat and admin tools to resolve issues faster
- Some fun stuff that is still in the works - will provide details at a later date :}

These changes should be already live at the time you are seeing this. Have fun and dont do anything too stupid!