---
title: Upgrades, people. Upgrades!
date: 2020-08-10
---
While these changes will not be physically visible on the server, you should see a large improvement to the server uptime and connection reliability. I have swapped out all our internet gear from the crappy, basic shit they give us, and replaced it with much more reliable and powerful gear that will help with all connections to the server.

- Changes:
	- New internet gear! Hopefully less time-out issues and downtime!
	- Updated the server OS to Ubuntu Server 20.04 - Hopefully this will improve performance a slight bit, but probably wont be noticeable. 
- Looking into:
	- More server events, more servers (ex: pvp, minigames, ARGs, etc.)
		- `QW4gQVJHIGlzIGluIHRoZSB3b3JrcyA6fQ==`
---
- Regarding HOSE...
	- I stated that I was trying to create my own Minecraft server executable to fix the many issues with java server implementation. Unfortunately, I have decided to discontinue HOSE.
	- The reason for discontinue is that a server version similar to HOSE is already in the works! It is called [Feather](https://github.com/feather-rs/feather/) and I am trying to contribute to it where I can! I hope to be able to move to this server in the future, as it runs very very fast.
	- **I would like to have some more stresstests as server events to play around and try to get the server to crash. I will announce these as development of [Feather](https://github.com/feather-rs/feather/) continues.**
		- I think it will be fun and a good server event to have once a month or so. It will help the community bond, and get closer to a lag-less server!

That's all for this changelog. It's been a hot sec, but again life is getting more and more busy. DM me with any changes, suggestions, or fun events you would like me to look into. Have fun yall!
