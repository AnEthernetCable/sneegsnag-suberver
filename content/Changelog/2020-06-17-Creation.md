---
title: This place now exists!
date: 2020-06-17
---
I've made this site so yall can see the work being done on the server, as well as get info about downtime, upgrades, and whatever else is happening!
It's not entirely complete, but slowly will become more filled. For now (unless you are seeing this in the future), its a bit empty.
Progress and pages will still be added so be sure to check back once in awhile!