---
title: Fixing the broken tings.
date: 2020-07-02
---
Tings arr broken. Still workin on da fixes. Lemme know what else is borked on discord pls :}.

- Changes:
	- Advanced Portals - Updated to latest version.
	- Chairs - Updated to latest version.
	- DeathMessagesPrime - Updated to latest version.
	- EssentialsX - Updated to latest version.
		- Should fix `/hat` issue.
	- OldCombatMechanics - Updated to latest version.
		- Fixed fishing rod not pulling players.
	- ShopKeepers - Updated to latest version.
	- WorldEdit - Updated to latest version.

- Looking into:
	- Silk Touch Spawner Plug-in.
		- Either looking for another plugin or trying to figure out the existing one.
	- Ender Pearls feel off.
	- Heads being reset.
		- This is a mojang issue and should be fixed in 1.16.2.

Changes are now live. Have fun and dont be too dumb :}.
